from django.forms.models import ModelForm
from django import forms
from .models import Student
from .models import CallofRoll

class StudentForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(StudentForm, self).__init__(*args, **kwargs)
        self.fields['birth_date'] = forms.DateField(widget=forms.TextInput(attrs={'type': 'date'}))
    class Meta:
        model = Student
        fields = (
            "first_name",
            "last_name",
            "birth_date",
            "email",
            "phone",
            "comments",
            "cursus",
        )

class CallofRollForm(ModelForm):
    #def init(self, *args, **kwargs):
     #   super(CallofRollForm, self).init(*args, **kwargs)
      #  student = Student.objects.get(pk=11) #self.request.POST.get('student'))
#        self.fields['student'] = student
    def __init__(self, cursus=None,*args, **kwargs):
        super(CallofRollForm, self).__init__(*args, **kwargs)
        self.fields['call_date'] = forms.DateField(widget=forms.TextInput(attrs={'type': 'date'}))

        self.fields['student'].required = False

    class Meta:
        model = CallofRoll
        fields = (
            "call_date",
            "student",
        )


class ParticularCallofRollForm(ModelForm):
    def __init__(self, cursus=None,*args, **kwargs):
        super(ParticularCallofRollForm, self).__init__(*args, **kwargs)
        self.fields['call_date'] = forms.DateField(widget=forms.TextInput(attrs={'type': 'date'}))
    class Meta:
        model = CallofRoll
        fields = (
            "call_date",
            "cursus",
            "student",
        )