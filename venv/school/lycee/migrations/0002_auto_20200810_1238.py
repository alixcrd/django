# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('lycee', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Cursus',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('name', models.CharField(max_length=50, null=True, default='aucun')),
                ('year_from_bac', models.SmallIntegerField(verbose_name='year', null=True, default=0, help_text='year since le bac')),
                ('scholar_year', models.CharField(max_length=9, null=True, default='0000-00001')),
            ],
        ),
        migrations.AddField(
            model_name='student',
            name='comments',
            field=models.CharField(verbose_name='comments', max_length=255, blank=True, default='', help_text='some comment about the student'),
        ),
        migrations.AddField(
            model_name='student',
            name='email',
            field=models.EmailField(verbose_name='email', max_length=255, default='x@y.z', help_text='email of the student'),
        ),
        migrations.AddField(
            model_name='student',
            name='last_name',
            field=models.CharField(verbose_name='lastname', max_length=255, default='???', help_text='last name of the student'),
        ),
        migrations.AddField(
            model_name='student',
            name='phone',
            field=models.CharField(verbose_name='phonenumber', max_length=10, default='0999999999', help_text='phone number of the student'),
        ),
        migrations.AddField(
            model_name='student',
            name='cursus',
            field=models.ForeignKey(null=True, to='lycee.Cursus'),
        ),
    ]
