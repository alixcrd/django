# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('lycee', '0002_auto_20200810_1238'),
    ]

    operations = [
        migrations.CreateModel(
            name='CallofRoll',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('call_date', models.DateField(verbose_name='call date')),
                ('cursus', models.ForeignKey(null=True, to='lycee.Cursus')),
            ],
        ),
        migrations.AlterField(
            model_name='student',
            name='email',
            field=models.EmailField(verbose_name='email', max_length=255, default='@limayrac.fr', help_text='email of the student'),
        ),
        migrations.AlterField(
            model_name='student',
            name='first_name',
            field=models.CharField(max_length=50, help_text='first name of the student'),
        ),
        migrations.AlterField(
            model_name='student',
            name='last_name',
            field=models.CharField(verbose_name='lastname', max_length=255, help_text='last name of the student'),
        ),
        migrations.AlterField(
            model_name='student',
            name='phone',
            field=models.CharField(verbose_name='phonenumber', max_length=10, help_text='phone number of the student'),
        ),
        migrations.AddField(
            model_name='callofroll',
            name='student',
            field=models.ForeignKey(null=True, to='lycee.Student'),
        ),
    ]
