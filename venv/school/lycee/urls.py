from django.conf.urls import url
from . import views
from .views import StudentCreateView
from .views import CallofRollView

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^(?P<cursus_id>[0-9]+)$', views.detail_cursus, name='detail_cursus'),
    url(r'^student/(?P<student_id>[0-9]+)$', views.detail_student, name='detail_student'),
    url(r'^student/create/$', StudentCreateView.as_view(), name='create_student'),
    url(r'^student/editstudent/(?P<pk>[0-9]+)$', views.EditStudentdataView.as_view(), name='editstudent'),
    url(r'^callofroll/(?P<cursus_id>[0-9]+)$', CallofRollView.as_view(), name='callofroll'),
    url(r'^callhistory/$', views.callhistory, name='callhistory'),
    url(r'^call/(?P<cursus_id>[0-9]+)$', views.calldetail, name='calldetail'),
    url(r'^particularcall/$', views.ParticularCallofRoll.as_view(), name='particularcall'),
]