from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import redirect
from django.template import loader
from .models import Cursus
from .models import Student
from .models import CallofRoll
from django.views.generic.edit import CreateView,UpdateView
from .forms import StudentForm
from .forms import CallofRollForm, ParticularCallofRollForm
from django.core.urlresolvers import reverse


def detail_cursus(request, cursus_id):
    result_list = Cursus.objects.get(pk=cursus_id)
    student_list = Student.objects.filter(cursus_id = cursus_id)
    context = {'cursus': result_list, 'student_list': student_list}
    return render(request, 'lycee/detail_cursus.html', context)

def index(request):
    result_list = Cursus.objects.order_by('name')
    template = loader.get_template('lycee/index.html')
    context = {
        'liste': result_list,
    }
    return HttpResponse(template.render(context, request))

def detail_student(request,student_id):
    result_list = Student.objects.get(pk=student_id)
    context = {'liste': result_list}
    return render(request, 'lycee/student/detail_student.html', context)

class StudentCreateView(CreateView):
    model = Student
    form_class = StudentForm
    template_name = "lycee/student/create.html"
    def get_success_url(self):
        return reverse("detail_student", args=(self.object.pk,))

class CallofRollView(CreateView):
    model = CallofRoll
    form_class = CallofRollForm
    template_name = "lycee/callofroll.html"
    def get_context_data(self, **kwargs):
        context = super(CallofRollView, self).get_context_data(**kwargs)
        students = Student.objects.filter(cursus_id=self.kwargs.get('cursus_id'))
        cursus = Cursus.objects.get(pk=self.kwargs.get('cursus_id'))
        context['cursus'] = cursus
        context['student_list'] = students
        return context
    def form_valid(self, form):
        cursus = Cursus.objects.get(pk=self.kwargs.get('cursus_id'))
        students = Student.objects.filter(cursus_id=self.kwargs.get('cursus_id'))
        student_list = self.request.POST.getlist('students')
        for student in students :
            if str(student.id) in student_list:
                student = Student.objects.get(pk=student.id)
                form = CallofRoll(call_date=self.request.POST.get('call_date'), cursus=cursus, student=student)
                form.save()
        return redirect('index')

def callhistory(request):
    call_list = CallofRoll.objects.all()
    context = {'call_list': call_list}
    return render(request, 'lycee/callhistory.html', context)

def calldetail(request,cursus_id):
    call = CallofRoll.objects.get(cursus_id=cursus_id)
    context = {'call': call}
    return render(request, 'lycee/call.html', context)

class EditStudentdataView(UpdateView):
    model = Student
    form_class = StudentForm
    template_name = "lycee/student/editstudent.html"
    def get_success_url(self):
        return reverse("detail_student", args=(self.object.pk,))

class ParticularCallofRoll(CreateView):
    model = CallofRoll
    form_class = ParticularCallofRollForm
    template_name = "lycee/particularcall.html"
    def get_success_url(self):
        return reverse("index")


